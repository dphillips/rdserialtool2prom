# rdserialtool2prom

This takes captured JSON output from [rdserialtool][rdserialtool] and pops it
into a Prometheus+Grafana stack for inspection.

Live capture and display of metrics is currently not supported. The closest you
can get is to re-export the JSON to openmetrics periodically and restart
Prometheus.

Sledgehammer-walnut perhaps but it's what worked for me in 30 minutes.

## Basic use

### 1. Capture some data using [rdserialtool][rdserialtool]

For example

	rdserialtool -d um24c -s /dev/rfcomm0 --json --watch > sla-discharge.json

### 2. Convert to openmetrics

The docker-compose.yaml expects the metrics at a certain path:

	./2prom.py < sla-discharge.json > compose/openmetrics

### 3. start/restart the stack

If you already have the stack running, you can restart only Prometheus to kick
it to using the new data. Otherwise, just start up the whole stack if this is
the first run:

	# start whole stack
	docker-compose --project-directory=compose up

	# or just restart prometheus if stack is already started
	docker-compose --project-directory=compose restart prometheus

Browse to Grafana on [localhost:3000](http://localhost:3000/) and find a
dashboard, or explore.

## To do

* Re-jig the volumes in docker-compose so they don't create empty root-owned
  directories on host when not existing.
* Implement live exporting to prometheus?
* Use a proper openmetrics library to avoid possibility of injection with
  crafted JSON

[rdserialtool]: https://github.com/rfinnie/rdserialtool
