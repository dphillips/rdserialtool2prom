#!/usr/bin/env python

import json
import sys


def emit_entry(f, d):
    prom_prefix = 'ruideng'
    inst_metrics = {
        'current_amperes': 'amps',
        'voltage_volts': 'volts',
        'power_watts': 'watts',
        'temperature_celsius': 'temp_c',
    }
    stamp = int(d['collection_time'])
    for prom_metric, key in inst_metrics.items():
        print(f'{prom_prefix}_{prom_metric}{{job="ruideng"}} {d[key]} {stamp}', file=f)

    group_metrics = {
        'charge_ampere_hours': 'amp_hours',
        'energy_watt_hours': 'watt_hours',
    }
    for n, group in enumerate(d['data_groups']):
        for prom_metric, key in group_metrics.items():
            print(f'{prom_prefix}_{prom_metric}{{job="ruideng",group="{n}"}} {group[key]} {stamp}', file=f)


def main():
    fin = sys.stdin
    fout = sys.stdout
    for line in fin:
        d = json.loads(line)
        emit_entry(fout, d)
    print('# EOF', file=fout)


if __name__ == "__main__":
    exit(main())
