#!/bin/sh -xe

rm -rf /prometheus/*
promtool tsdb create-blocks-from openmetrics /openmetrics /prometheus
exec prometheus \
	--config.file=/etc/prometheus/prometheus.yml \
	--storage.tsdb.path=/prometheus \
	--web.console.libraries=/usr/share/prometheus/console_libraries \
	--web.console.templates=/usr/share/prometheus/consoles \
	--storage.tsdb.retention.size=50GB
